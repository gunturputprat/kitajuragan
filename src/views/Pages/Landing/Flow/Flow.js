import React, { Component } from 'react';

class Flow extends Component {
  render() {
    return (
        <div id="flow" className="flow">
          <p className="flow-title">Alur Pemberdayaan</p>
           <div className="flow-border">
            <img className="flow-img" src="../../../../../assets/flow/flow.png"/>
           </div>
        </div>

    );
  }
}

export default Flow;
