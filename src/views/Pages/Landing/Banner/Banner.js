import React, { Component } from 'react';
import firebase from '../../../firebase'
import { Link } from 'react-router-dom';

class Banner extends Component {
  render() {
    return (
        <div id="banner" className="banner">
            <div className="banner-left">
            <p className="title-tagline">Berdayakan Usaha Mikro di Daerah Urban</p>
            <div className="btn-invest">
            <Link to='/login'><button className="btn-invest-now">Investasi Usaha Mikro</button></Link>
            {/* <button className="btn-invest-now" onClick={loginGoogle}>Investasi Usaha Mikro</button> */}
            <br></br>
            <button className="btn-invest-help" onClick={helpByWA}>Bantuan Berinvestasi</button>
            </div>
            </div>
            <div className="banner-right">
            <img class="banner-img" src="../../../../../assets/banner/banner.png"/>
            </div>
        </div>

    );
  }
}
const provider = new firebase.auth.GoogleAuthProvider();

function helpByWA() {
  return(
    window.location.href= "https://api.whatsapp.com/send?phone=628119408110&text=Hallo%20Admin,%20saya%20mau%20berinvestasi%20di%20Kita%20Juragan,%20bagaimana%20caranya?"
  )
}

export default Banner;
