import React, { Component } from 'react';

class Media extends Component {
  render() {
    return (
      <div id="media" className="media">
          <div className="media-title">Liputan Media
          <div className="media-border">
            <a target="_blank" href="https://www.indozone.id/news/qEsy5d/meski-bertampang-sangar-aksi-anak-punk-ini-bikin-salut"><img className="media-img" src="../../../../../assets/media/indozone1.png"/></a>
            <a target="_blank" href="https://hot.liputan6.com/read/3972878/bertampang-sangar-anak-punk-ini-lakukan-kegiatan-sosial-yang-bikin-salut"><img className="media-img"  src="../../../../../assets/media/liputan61.png"/></a>
            <a target="_blank" href="https://drive.google.com/open?id=1FRwkhBG6XenYuH_cZSTv2OOOh-7FgyPg"><img className="media-img"  src="../../../../../assets/media/radardepok1.png"/></a>
            <a target="_blank" href="https://www.tokohkita.co/read/20190820/770/keseruan-olimpiade-satu-tuju-an-gawean-komunitas-depok-banget"><img className="media-img"  src="../../../../../assets/media/tokohkita1.png"/></a>
          </div>
          </div>
      </div>
    );
  }
}

export default Media;
