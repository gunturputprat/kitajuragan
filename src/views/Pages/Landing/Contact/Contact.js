import React, { Component } from 'react';

class Contact extends Component {
  render() {
    return (
        <div id="contact" className="contact">
           <div className="contact-left">
             <img class="contact-img-logo" src="../../../../../assets/logo/logo.png"/>
           </div>
           <div className="contact-right">
              <div clasName="contact-socialmedia">
                <a target="_blank" href="https://www.instagram.com/kita.juragan">
                <img className="contact-img" src="../../../../../assets/socialmedia/ig.png"/>
                <div className="contact-mediasocial-title">Kita.juragan</div>
              </a>
              </div>
              <div clasName="contact-socialmedia">
                <a target="_blank" href="https://api.whatsapp.com/send?phone=628119408110&text=Halo%20Admin%20Kitajuragan">
                <img className="contact-img" src="../../../../../assets/socialmedia/wa.png"/>
                <div className="contact-mediasocial-title">0811-9408-110</div>
                </a>
              </div>
              <div clasName="contact-socialmedia">
                <a target="_blank" href="https://www.youtube.com/channel/UCLwwf-HAYtXP3COfF-SIdpA">
                <img className="contact-img" src="../../../../../assets/socialmedia/youtube.png"/>
                <div className="contact-mediasocial-title">KITA JURAGAN</div>
                </a>
              </div>
              <div clasName="contact-socialmedia">
                <img className="contact-img" src="../../../../../assets/socialmedia/mail.png"/>
                <div className="contact-mediasocial-title">kitajuraganofficial@gmail.com</div>
              </div>
           </div>
        </div>

    );
  }
}

export default Contact;
