import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import AppBarCollapse from "./AppBarCollapse";
class Header extends Component {
  render() {
    return (
      <AppBar position="fixed" className="navigation">
      <Toolbar className="header">
        <Button href="/"><img className="logo" src="../../../../../assets/logo/logo_kj_black.png"/></Button>
        <div className="btn-parent-left">
          <Button href="#advantage" color="inherit">Manfaat</Button>
          <Button href="#flow" color="inherit">Alur Pemberdayaan</Button>
          <Button href="#achievement" color="inherit">Penghargaan</Button>
          <Button href="#media" color="inherit">Liputan Media</Button>
          <Button href="#contact" color="inherit">Kontak</Button>
        </div>
        {/* <AppBarCollapse></AppBarCollapse> */}
        {/* <Button variant="contained" onClick={logout}>Logout</Button> */}
      </Toolbar>
    </AppBar>
    );
  }
}
export default Header;
