/**
 * Code from the below medium post, only updated for latest material UI, using a
 * Menu for the popup and with breakpoints that work.
 *
 * https://medium.com/@habibmahbub/create-appbar-material-ui-responsive-like-bootstrap-1a65e8286d6f
 */
import React from "react";
import { Button, MenuItem } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import ButtonAppBarCollapse from "./ButtonAppBarCollapse";

const styles = theme => ({
  root: {
    position: "absolute",
    right: 0
  },
  buttonBar: {
    [theme.breakpoints.down("xs")]: {
      display: "none"
    },
    margin: "10px",
    paddingLeft: "16px",
    right: 0,
    position: "relative",
    width: "100%",
    background: "transparent"
  }
});

const AppBarCollapse = props => (
  <div className={props.classes.root}>
    <ButtonAppBarCollapse>
      <Button href="#advantage">Manfaat</Button>
      <Button href="#flow">Alur Pemberdayaan</Button>
      <Button href="#achievement">Penghargaan</Button>
      <Button href="#media">Liputan Media</Button>
      <Button href="#contact">Kontak</Button>    </ButtonAppBarCollapse>
    <div className={props.classes.buttonBar} id="appbar-collapse">
      <Button href="#advantage" color="inherit">Manfaat</Button>
      <Button href="#flow" color="inherit">Alur Pemberdayaan</Button>
      <Button href="#achievement" color="inherit">Penghargaan</Button>
      <Button href="#media" color="inherit">Liputan Media</Button>
      <Button href="#contact" color="inherit">Kontak</Button>
    </div>
  </div>
);

export default withStyles(styles)(AppBarCollapse);
