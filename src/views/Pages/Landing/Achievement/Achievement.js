import React, { Component } from 'react';

class Achievement extends Component {
  render() {
  return (
    <div id="achievement" className="achievement">
        <p className="achievement-title">Penghargaan</p>
        <div className="achievement-border">
          <a target="_blank" href="https://www.instagram.com/p/B2tFfAIhPop/?utm_source=ig_web_copy_link"><img className="achievement-img" src="../../../../../assets/achievement/achievement-sdg-border.png"/></a>
        </div>
        <div className="achievement-border">
          <a target="_blank" href="https://www.instagram.com/p/B532-JGBFSO/?utm_source=ig_web_copy_link"><img className="achievement-img" src="../../../../../assets/achievement/achievement-jakbee-border.png"/></a>
        </div>
        <div className="achievement-border">
          <a target="_blank" href="https://www.instagram.com/p/B2yFd1LBW7T/?utm_source=ig_web_copy_link"><img className="achievement-img" src="../../../../../assets/achievement/achievement-incubated-border.png"/></a>
        </div>
    </div>
  );
}
  
}
export default Achievement;
