import React, { Component } from 'react';
import Container from '@material-ui/core/Container';

class Advantage extends Component {
  render() {
    return (
        <div id="advantage" className="advantage">
            <p className="advantage-title">Manfaat</p>
            <div className="advantage-card">
                <div className="advantage-img">
                    <img width="200" height="150" src="../../../../../assets/advantage/poverty.png"/>
                </div>
                <div className="advantage-card-description desc1">Mengentaskan kemiskinan di Daerah urban</div>
            </div>
            <div className="advantage-card">
                <div className="advantage-img">
                    <img width="200" height="150" src="../../../../../assets/advantage/profit.png"/>
                </div>
                <div className="advantage-card-description desc2">Bagi hasil yang menguntungkan</div>
            </div>
            <div className="advantage-card">
                <div className="advantage-img">
                    <img width="200" height="150" src="../../../../../assets/advantage/check.png"/>
                </div>
                <div className="advantage-card-description desc3">Mudah, tepat, dan aman</div>
            </div>
        </div>

    );
  }
}

export default Advantage;