import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

import { Link } from 'react-router-dom';
import Header from './Header/Header';
import Banner from './Banner/Banner';
import Advantage from './Advantage/Advantage';
import Flow from './Flow/Flow';
import Achievement from './Achievement/Achievement';
import Media from './Media/Media';
import Contact from './Contact/Contact';

class Landing extends Component {
  render() {
  return (
    <div className="root">
      {/* header */}
      <Header></Header>
      {/* banner */}
      <Banner></Banner>
      {/* advantage */}
      <Advantage></Advantage>
      {/* flow */}
      <Flow></Flow>
      {/* achievement */}
      <Achievement></Achievement>
      {/* media */}
      <Media></Media>
      {/* contact */}
      <Contact></Contact>
      {/* copyrights */}
      <div className="copyright">
      <b>© {new Date().getFullYear()} Kitajuragan.com |</b> Created by <a target="_blank" href="https://defuture.tech">Defuture Technology</a>
      </div>
      
    </div>
  );
}
  
}

export default Landing;
