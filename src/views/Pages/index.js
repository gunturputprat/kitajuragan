import Login from './Login';
import Page404 from './Page404';
import Page500 from './Page500';
import Register from './Register';
import Juragan from './Juragan';

export {
  Login, Page404, Page500, Register, Juragan
};