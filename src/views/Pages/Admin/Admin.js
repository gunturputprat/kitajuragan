import React, { Component } from 'react';
import firebase from '../../firebase'

import { Button, Container} from 'reactstrap';
import Header from './Header/Header';

class Admin extends Component {
  componentDidMount() {
    if(localStorage.getItem('isLoggedin') === 'true') {
      this.props.history.push('/juragan');
    }
  }
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container className="auth-container">
          <Header></Header>
          <div className="auth-bg">
            <img className="auth-img" width="500" src="../../../assets/auth/login-img.png"/>
            <h1 className="auth-title">Jadi JURAGAN HARI INI!</h1>
              <Button onClick={loginWithGoogle} color="primary" active tabIndex={-1}><img width="20" src="../../../../assets/logo/google.png"/>  Masuk dengan Google</Button>
          </div>
        </Container>
      </div>
    );
  }
}

const provider = new firebase.auth.GoogleAuthProvider();

function loginWithGoogle() {
  return (
    firebase.auth().signInWithPopup(provider).then((res) => {
      const user = {
        displayName:res.user.displayName,
        email:res.user.email,
        photoURL:res.user.photoURL,
        emailVerified:res.user.emailVerified,
        phoneNumber:res.user.phoneNumber,
        uid:res.user.uid,
        refreshToken:res.user.refreshToken,
        created_at: new Date()
      }
      // save new user to database
      firebase.firestore().collection('users').doc(res.user.uid).set({
          displayName:res.user.displayName,
          email:res.user.email,
          photoURL:res.user.photoURL,
          emailVerified:res.user.emailVerified,
          phoneNumber:res.user.phoneNumber,
          uid:res.user.uid,
          refreshToken:res.user.refreshToken
        }).then(function() {
          localStorage.setItem("currentUser", JSON.stringify(user))
          localStorage.setItem('isLoggedin', 'true')
          this.props.history.push('/juragan')
        })
        .catch(function(error) {
            alert("Kamu gagal mengisi form. Silahkan coba lagi :)");
        });
    })
    .catch((err) => {
      console.log(err)
    })
  )
}
export default Admin;
