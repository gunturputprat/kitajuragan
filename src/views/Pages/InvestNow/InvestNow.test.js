import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import InvestNow from './InvestNow';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><InvestNow/></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
