import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';
import { Button} from 'reactstrap';

import firebase from '../../firebase'

import Contact from '../Landing/Contact/Contact';

class InvestNow extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     user:JSON.parse(localStorage.getItem("currentUser"))
  //   }
  // }

  render() {
    // const displayName = this.state.user.displayName
    return (
      <div>
      {/* header */}
      <AppBar position="static">
        <Toolbar className="header">
        <Link to="/"><img className="logo" src="../../../../../assets/logo/logo_kj_black.png"/></Link>          
        <Link to="/" variant="contained"><Button className="btn-logout" onClick={logout}>Keluar</Button></Link>
        </Toolbar>
      </AppBar>
      <Container className="invest-container">
        <div className="invest-card">
          <div className="invest-juragan"></div>
          <div className="invest-transfer">
            <p>Transfer</p>
            <p>Transfer ke Nomor Kekening</p>
            <img src="../../../../../assets/icon/logo_bni_syariah.png"/>
            <p>089-709-5661<span>(Muhammad Akbar Buana)</span></p>
            <p>Jumlah yang harus dibayar :<span>Rp. 5.000.000</span></p>
            <div className="confirm-alert"></div>
          </div>
        </div>
        <div className="invest-description">
          <p>Keterangan</p>
          <p>Batas waktu transfer 1x24 jam.<br></br>
             Uang yang di transfer sepenuhnya untuk modal usaha mikro.</p>
          <Link to="/thankyou"><Button className="btn-invest-status">Selesai</Button></Link>
        </div>
      </Container>
      <Contact></Contact>
      </div>
    );
  }
}
function logout() {
  firebase.auth().signOut().then((res) => 
  localStorage.clear()
  )
    .catch((err) => console.log(err))
}
export default InvestNow;