import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Juragan from './Juragan';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MemoryRouter><Juragan/></MemoryRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
