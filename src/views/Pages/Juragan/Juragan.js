import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import JuraganCard from '../../Components/JuraganCard/JuraganCard'
import { Button} from 'reactstrap';
import Contact from '../Landing/Contact/Contact'
import { Link } from 'react-router-dom';

import firebase from '../../firebase'

class Juragan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user:JSON.parse(localStorage.getItem("currentUser")),
      showDashboard:false
    }
  }
  componentDidMount() {
    console.log("ks",this.state.user.email)
    if(this.state.user.email === "kitajuraganofficial@gmail.com" || this.state.user.email === "tafsiliakbar@gmail.com" || this.state.user.email === "gppratama175@gmail.com") {
      this.setState({showDashboard:true})
    }
  }

  render() {
    // const displayName = this.state.user.displayName
    return (
      <div>
      {/* header */}
      <AppBar position="static">
        <Toolbar className="header">
        <Link to="/" className="btn-logo-juragan" ><img className="logo" src="../../../../../assets/logo/logo_kj_black.png"/></Link>
        {this.state.showDashboard ? 
        <Link to='/dashboard-kita-juragan' className="btn-dashboard">Dashboard</Link>
        : null
        }
        <Link to="/" variant="contained"><Button className="btn-logout" onClick={logout}>Keluar</Button></Link>
        </Toolbar>
      </AppBar>
      <JuraganCard></JuraganCard>
      <Contact></Contact>
      </div>
    );
  }
}

function logout() {
  firebase.auth().signOut().then((res) => 
  localStorage.clear()
  )
    .catch((err) => console.log(err))
}
export default Juragan;