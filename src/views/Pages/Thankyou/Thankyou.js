import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';
import { Button} from 'reactstrap';
import Contact from '../Landing/Contact/Contact';

class Thankyou extends Component {
  render() {
    return (
      <div>
      {/* header */}
      <AppBar position="static">
        <Toolbar className="header" >
          <IconButton edge="start" className="menuButton" color="inherit" aria-label="menu">
          <Link to="/" className="btn-logo-juragan"><img className="logo" src="../../../../../assets/logo/logo_kj_black.png"/></Link>
          </IconButton>
          {/* <Button variant="contained" onClick={logout}>Logout</Button> */}
        </Toolbar>
      </AppBar>
        <img className="thankyou-img" src="../../../../../assets/banner/thankyou.png"/>
      <Contact></Contact>
      </div>
    );
  }
}

export default Thankyou;