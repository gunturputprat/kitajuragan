import React, { Component } from 'react';
import firebase from '../../firebase';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

// import Modal from '@material-ui/core/Modal';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
      
class JuraganCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
          boards:[],
          avatarURL:[],
          length:'',
          modal:false,
          index:0
        }    
      }

      componentDidMount() {
        const db = firebase.firestore();
        const boards = [];
        
        db.collection("listJuragan").get().then((querySnapshot) => {
          const length = querySnapshot.size
          querySnapshot.forEach((doc) => {
            const { fullname, picture, birthday, description, place, created_at } = doc.data();
            boards.push({
              key: doc.id,
              fullname,
              picture,
              description,
              place,   
              created_at,         
              birthday,
            })
            })
            this.setState({boards,length:length});
          })
            .catch(function(error) {
            //   console.log("Error getting documents: ", error);
          })
      }
    
    showModal(x){
        this.setState({modal:true,index:x})
        // console.log(x)
    }
    finish(juragan){
        const currentUser = JSON.parse(localStorage.getItem("currentUser"))
        // console.log("c",currentUser.uid)
        this.setState({modal:false})
        // save new investor to db
        firebase.firestore().collection('listInvestor').add({
            uid:currentUser.uid,
            created_at: new Date(),
            invest_to:juragan
          }).then(function() {
            
        })
          .catch(function(error) {
              alert("Kamu gagal mengisi form. Silahkan coba lagi :)");
          });
        
    }
    closeModal(){
        this.setState({modal:false})
    }
    
    render() {
        return (
            <div className="juragan-container">
                {/* juragan 1 */}
                {this.state.boards.map(board =>
                <div className="juragan-card">
                    <div>
                        <img className="juragan-img" src={board.picture}/>
                    </div>
                    <div className="juragan-typography">

                    <Typography gutterBottom variant="h5" component="h2">{board.fullname}</Typography>
                    <p>{board.place}, {board.birthday}</p>
                    <Typography className="juragan-description" variant="body2" color="textSecondary" component="p">
                    {board.description}
                    </Typography>
                    </div> 
                    <Button className="btn-invest-juragan"  onClick={()=>this.showModal(board)}>Beri investasi</Button>
                </div>
                )}

            <Modal size="lg" isOpen={this.state.modal}>
                <ModalHeader toggle={()=>this.closeModal()} className="modals">{this.state.index.fullname}</ModalHeader>
                <ModalBody>
                    <div className="center">
                        <img className="modal-img" src={this.state.index.picture}/>
                    </div>
                    <div className="modal-left">
                        <p>
                            <b>Asal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> {this.state.index.place}, {this.state.index.birthday}<br></br>
                            <b>Deskripsi :</b> <span className="grey">{this.state.index.description}</span>
                        </p>
                    </div>
                    <div className="modal-right">
                        <p className="grey"><b>Transfer ke Nomor Rekening</b><br></br><br></br>
                        <img className="bank-transfer" src="../../../assets/icon/logo_bni_syariah.png"/>
                        <p className="no-rek"><b>089-709-5661</b><br></br><span className="grey name-rek">(Muhammad Akbar Buana)</span></p>
                        <br></br>
                        <span className="grey">Jumlah yang harus dibayar :</span><br></br>
                        <span className="amount">Rp. 5.000.000</span></p>
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=628119408110&text=Hallo%20Admin,%20saya%20ingin%20konfirmasi%20setelah%20transfer%20investasi%20Usaha%20Mikro,%20bagaimana%20caranya?"><div className="confirm-alert">Konfirmasi setelah transfer</div></a>
                    </div>
                </ModalBody>
                <div className="note">
                    <h2><b>Keterangan</b></h2>
                    Batas waktu transfer 1x24 jam.<br></br>Uang yang di transfer sepenuhnya untuk modal usaha mikro
                </div>
                <ModalFooter>
                <Link to="/thankyou"><Button className="btn-invest-finish" onClick={()=>this.finish(this.state.index.fullname)}>Selesai</Button></Link>
                </ModalFooter>
                
            </Modal>
            </div>
        );
    }
}
export default JuraganCard;