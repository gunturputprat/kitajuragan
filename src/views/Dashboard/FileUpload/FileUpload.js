import React, { Component} from 'react';
import firebase from '../../../views/firebase'
import FileUploader from 'react-firebase-file-uploader';


class FileUpload extends Component {

  state = {
    username: '',
    avatar: '',
    isUploading: false,
    progress: 0,
    avatarURL: ''
  };

  handleChangeUsername = (event) => this.setState({username: event.target.value});
  handleUploadStart = () => this.setState({isUploading: true, progress: 0});
  handleProgress = (progress) => this.setState({progress});
  handleUploadError = (error) => {
  this.setState({isUploading: false});
  console.error(error);
  }
  handleUploadSuccess = (filename) => {
  this.setState({avatar: filename, progress: 100, isUploading: false});
  firebase.storage().ref('images').child(filename).getDownloadURL().then(url => this.setState({avatarURL: url}));
  };
render() {

  // console.log("pro", this.state.progress)
  return (
    <div className="App">
      {/* Add Juragan */}
      {/* <Modal></Modal> */}

      <form 
            onSubmit={e => {
              const date = new Date()
              const month = '' + (date.getMonth() + 1)
              const day = '' + date.getDate()
              const year = date.getFullYear()
              const date2 = day+'-'+month+'-'+year
              
              e.preventDefault();
              firebase.firestore().collection('listJuragan').add({
                fullname:e.target.fullname.value,
                place:e.target.place.value,
                birthday:e.target.birthday.value,
                description:e.target.description.value,
                picture:this.state.avatarURL,
                created_at: date2
                }).then(function() {
                  alert("Selamat! Kamu berhasil menambahkan juragan.");
                  window.location.reload(); 
                })
                .catch(function(error) {
                    alert("Kamu gagal mengisi form. Silahkan coba lagi :)");
                });
  
              }}
            
            className="root" noValidate autoComplete="on">
              <div>
                <p>
              <label className="form-label" htmlFor="fullName">Nama lengkap<span className="mandatory">*</span></label><br></br>
              <input className="form-input" required name="fullname" type="text" placeholder="Nama lengkap"/>
                </p>
                <p>
                  <label className="form-label" htmlFor="place">Tempat Lahir<span className="mandatory">*</span></label><br></br>
                  <input className="form-input" required name="place" type="text" placeholder="Tempat lahir"/>
                </p>
                <p>
                  <label className="form-label" htmlFor="birthday">Tanggal lahir<span className="mandatory">*</span></label><br></br>
                  <input className="form-input" required name="birthday" format="DD-MMMM-YYYY" type="date" placeholder="Tanggal lahir"/>
                </p>
                <p>
                  <label className="form-label" htmlFor="description">Deskripsi<span className="mandatory">*</span></label><br></br>
                  <textarea rows="3" cols="22" className="form-input" required name="description" type="text" placeholder="Deskripsi"/>
                </p>
                <label className="form-label">Foto juragan :</label><br></br>
                  {this.state.isUploading &&
                    <p>Progress: {this.state.progress}</p>
                  }
                {this.state.avatarURL &&
                 <img height="50" width="50" src={this.state.avatarURL} />
                }
                <FileUploader
                accept="image/*"
                name="avatar"
                randomizeFilename
                storageRef={firebase.storage().ref('images')}
                onUploadStart={this.handleUploadStart}
                onUploadError={this.handleUploadError}
                onUploadSuccess={this.handleUploadSuccess}
                onProgress={this.handleProgress}
                />
                {/* <TextField required id="standard-basic" label="Name" value={this.state.name}/>
                <TextField required id="standard-basic" label="Place" value={this.state.place}/>
                <TextField required id="standard-basic" label="Birthday" type="date" value={this.state.birthday}/>
                <TextField required id="standard-basic" label="Picture" value={this.state.picture}/>
                <TextareaAutosize rowsMax={4} aria-label="maximum height" placeholder="Maximum 4 rows" value={this.state.description}/> */}
              </div>
              <button className="btn-upload-picture" value="submit" type="submit">Tambah Juragan</button>
            </form>
    </div>
  );
}
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

}


export default FileUpload;
