import React, { Component} from 'react';
import firebase from '../../../views/firebase'


class JuraganList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boards:[],
      avatarURL:[],
      length:''
    }    
  }
  componentDidMount() {
    const db = firebase.firestore();
    const boards = [];
    db.collection("listJuragan").get().then((querySnapshot) => {
      const length = querySnapshot.size
      querySnapshot.forEach((doc) => {
        const { fullname, picture, birthday, description, place, created_at } = doc.data();
        boards.push({
          key: doc.id,
          fullname,
          picture,
          description,
          place,   
          created_at,         
          birthday,
        })
        })
        this.setState({boards,length:length});
      })
        .catch(function(error) {
          // console.log("Error getting documents: ", error);
      })
  }


  render() {
    // console.log("boards",this.state.boards)
    const length = this.state.length
    return (
      <div>
          <p>Jumlah juragan terdaftar adalah <b>{length} orang</b></p>
          {/* list juragan */}
          {this.state.boards.map(board =>
          <div className="juragan-list-card">
              <img className="juragan-list-img" src={board.picture}/>
              <div className="juragan-list-detail">
                <p>
                  Name      : <b>{board.fullname}</b>
                  <button className="btn-list-delete" onClick={() => this.deleteJuragan(board.key)}>Hapus</button>
                  <br></br>
                  TTL       : {board.place}, {board.birthday}<br></br>
                  Bergabung : {board.created_at}<br></br>
                  Deskripsi : {board.description}
                </p>
              </div>
          </div>
        )}
      </div>
    );

  }
  // delete juragan from list
  deleteJuragan(key) {
    firebase.firestore().collection('listJuragan').doc(key).delete().then(function() {
      alert("Berhasil menghapus satu juragan")
      // console.log("Document successfully deleted!");
      window.location.reload()
    }).catch(function(error) {
      console.error("Error removing document: ", error);
    });
  }}
export default JuraganList;