import React, { Component, lazy, Suspense } from 'react';
import Modal from '../Components/JuraganModal/JuraganModal';
import firebase from '../../views/firebase'
import { makeStyles } from '@material-ui/core/styles';
import FileUploader from 'react-firebase-file-uploader';
import FileUpload from './FileUpload/FileUpload';
import JuraganList from './JuraganList/JuraganList';

class Dashboard extends Component {

  state = {
    username: '',
    avatar: '',
    isUploading: false,
    progress: 0,
    avatarURL: ''
  };

  handleChangeUsername = (event) => this.setState({username: event.target.value});
  handleUploadStart = () => this.setState({isUploading: true, progress: 0});
  handleProgress = (progress) => this.setState({progress});
  handleUploadError = (error) => {
  this.setState({isUploading: false});
  console.error(error);
  }
  handleUploadSuccess = (filename) => {
  this.setState({avatar: filename, progress: 100, isUploading: false});
  firebase.storage().ref('images').child(filename).getDownloadURL().then(url => this.setState({avatarURL: url}));
  };
render() {

  return (
    <div className="App">
      {/* Add Juragan */}
      <div className="container-fileupload">
        <FileUpload></FileUpload>
      </div>
      <div className="juragan-list">
        <JuraganList></JuraganList>
      </div>
    </div>
  );
}
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
}


export default Dashboard;
