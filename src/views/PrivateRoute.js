import React from 'react';
import { Route, Redirect } from 'react-router-dom';
const Juragan = React.lazy(() => import('./Pages/Juragan'));

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props, ref) => (
        localStorage.getItem('isLoggedin')
            ? 
            <Juragan>
            </Juragan>
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )} 
    />
)