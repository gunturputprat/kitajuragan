import * as firebase from 'firebase';
// import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const config = {
    apiKey: "AIzaSyC5V-szWiQlkf7atoslVKedwCKQy2kRBm8",
    authDomain: "kitajuragan-83fb0.firebaseapp.com",
    databaseURL: "https://kitajuragan-83fb0.firebaseio.com",
    projectId: "kitajuragan-83fb0",
    storageBucket: "kitajuragan-83fb0.appspot.com",
    messagingSenderId: "731461374517",
    appId: "1:731461374517:web:923cadb853c81c8f30177c",
    measurementId: "G-Z3QDBJ2VYJ"
};

firebase.initializeApp(config);
firebase.analytics();
firebase.storage();
firebase.firestore().settings(settings);


export default firebase;
// const app = lazy(() => firebase.initializeApp(config));
// const analytics = lazy(() => firebase.analytics();
// const fire = lazy(() => firebase.firestore().settings(settings);
// const auth = lazy(() =>  app().auth());
// export { auth, app, analytics, fire};