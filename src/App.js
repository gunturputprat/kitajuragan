import React, { Component } from 'react';
import { HashRouter, BrowserRouter,Route, Switch } from 'react-router-dom';
// import { renderRoutes } from 'react-router-config';
import './App.scss';

import { PrivateRoute } from './views/PrivateRoute';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const Login = React.lazy(() => import('./views/Pages/Login'));
const Juragan = React.lazy(() => import('./views/Pages/Juragan'));
const InvestNow = React.lazy(() => import('./views/Pages/InvestNow'));
const Thankyou = React.lazy(() => import('./views/Pages/Thankyou'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));
const Landing = React.lazy(() => import('./views/Pages/Landing'));
// const Admin = React.lazy(() => import('./views/Pages/Admin'));

class App extends Component {

  render() {
    return (
      <HashRouter>
          <React.Suspense fallback={loading()}>
            <Switch>
            <Route exact path="/" name="Landing" render={props => <Landing {...props}/>} />
            <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
            <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
            <PrivateRoute path="/juragan" name="Juragan" render={props => <Juragan {...props}/>} />
            <PrivateRoute path="/invest-now" name="InvestNow" render={props => <InvestNow {...props}/>} />
            <Route path="/dashboard-kita-juragan" name="Dashboard" render={props => <DefaultLayout {...props}/>} />
            <Route path="/thankyou" name="Thankyou" render={props => <Thankyou {...props}/>} />
            <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
            <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
            {/* <Route exact path="/admin-juragan" name="Admin Page" render={props => <Admin {...props}/>} /> */}
            </Switch>
          </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
